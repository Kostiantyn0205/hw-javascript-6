/*1) Це дозволяє нам використовувати спец. символи так, що вони розпізнаються як текст, а не частина коду. Символ екранування \*/
/*2) Function Expression, Function Declaration, Стрілкова функція*/
/*3) Це дозволяє використовувати змінні та функції до їх оголошення. Коли компілюється код, всі оголошення змінних та функцій піднімаються до початку коду,
тому їх можна використовувати до моменту оголошення. Однак, при використанні let та const їх не можна використовувати до оголошення у коді. Це може призвести до помилок в коді*/

const createNewUser = function () {
    const newUser = {
        _firstName: "",
        _lastName: "",
        birthday: "",
        get login() {
            return `${this._firstName[0].toLowerCase()}${this._lastName.toLowerCase()}`;
        },
        set firstName(newName) {
            this._firstName = newName;
        },
        set lastName(newName) {
            this._lastName = newName;
        },
        getAge() {
            const today = new Date();
            const [day, month, year] = this.birthday.split(".");
            const birthDate = new Date(`${year}-${month}-${day}`);
            let age = today.getFullYear() - birthDate.getFullYear();
            const monthDiff = today.getMonth() - birthDate.getMonth();
            const dayDiff = today.getDate() - birthDate.getDate()
            if (monthDiff < 0 || (monthDiff === 0 && dayDiff < 0)) {
                age--;
            }
            return age;
        },
        getPassword() {
            return `${this._firstName[0].toUpperCase()}${this._lastName.toLowerCase()}${this.birthday.slice(-4)}`;
        }
    };

    newUser.firstName = prompt("Введіть своє ім'я:");
    newUser.lastName = prompt("Введіть своє прізвище:");
    newUser.birthday = prompt("Введите свою дату рождения в формате 'dd.mm.yyyy':");

    return newUser;
};

const user = createNewUser();
console.log(`Логін користувача: ${user.login}`);
console.log(`Вік користувача: ${user.getAge()}`);
console.log(`Пароль користувача: ${user.getPassword()}`);